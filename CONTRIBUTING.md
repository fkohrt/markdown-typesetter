# Contributing

You want to contribute? Go ahead! Source is available under the MIT license at [`gitlab.com/fkohrt/markdown-typesetter`](https://gitlab.com/fkohrt/markdown-typesetter).

# Developer information

## Deployment

### Always

```r
options(repos = c(CRAN = "https://cloud.r-project.org"))
update.packages(ask = FALSE, lib.loc = Sys.getenv("R_LIBS_USER"))
# install required packages locally, e.g.:
install.packages(c("reticulate", "kableExtra", "formatR", "rsvg"))
# only in case of issues due to a recent pandoc change:
# remotes::install_github("rstudio/rmarkdown")
Sys.setlocale('LC_ALL', 'de_DE.UTF-8')
Sys.setenv(LANG = "de_DE.UTF-8")
# adjust the following with your credentials:
# rsconnect::setAccountInfo(name='USERNAME', token='TOKEN', secret='SECRET')
rsconnect::deployApp("markdown-typesetter", forceUpdate = TRUE)
```

### First time

- on the settings page of your shinyapps.io application
  - increase `Startup Timeout` from 60 to ~240 seconds
  - decrease `Max Connections` from 50 to ~5
  - decrease `Instance Idle Timeout` from 15 to 5 minutes

## `@` references

The source documents contains various `@` references that connect scattered settings that belong together. Removing a single one of the group may affect the functioning of all of them, all references are explained in this document.

### Automatic download of citation styles and pandoc filters @download

Some citation files and necessary pandoc filters are downloaded automatically so you'll always have the newest version.

### License @metadata

The parameter `license` defines a [SPDX identifier](https://spdx.org/licenses/). Setting this will include textual and visual indicators and metadata will be set accordingly. Currently the only supported licenses are Creative Commons 4.0 licenses and CC0 1.0.

### Metadata @metadata

Aside from the standard metadata included in the document information dictionary (such as title, author, subject and keywords; see ISO 32000-1), PDFs will contain embedded [XMP metadata](https://wiki.creativecommons.org/wiki/XMP) including the title, author and license information. HTML output will store license information through [RDFa](https://wiki.creativecommons.org/wiki/RDFa).


### Localization @quotes

PDFs (through the package `csquotes` and metadata field `csquotes`) as well as HTML output (through the Pandoc filter [`pandoc-quotes.lua`](https://github.com/pandoc/lua-filters/tree/master/pandoc-quotes.lua)) will have typewriter quotes `""` turned into typographic quotes `„“`, honoring the language metadata field `lang`.

### Print support @print

When planning for PDF print publication, one may enable the params `show_urls_in_footnotes` (which should be used instead of Pandoc's metadata field `links-as-notes`) and/or `show_url_break_symbols`. This is implemented by the chunks `show_url_break_symbols`, `footnotes_and_symbols`, `only_symbols` and `only_footnotes`.

### Debugging ready @debug

Turn those swiches and look through all intermediary files.

### Code listings @code

Code listings are set up with automatic line breaks.

### @blockquote style

Blockquotes are styled to be more recognizable, code taken from [Eisvogel template](https://github.com/Wandmalfarbe/pandoc-latex-template).

### Draft mode @draft

Enabling the draft mode adds visual indication that the document is unfinished.

### hidelinks @template

Patching newest [`default.latex`](https://github.com/jgm/pandoc-templates/blob/master/default.latex)...

- ...to remove `hidelinks` default for the `hyperref` package and make it configurable (hidelinks draws borders around links that are only visible in the digital version, not when printed)
- ...to enable monospaced font for URLs by removing `\urlstyle{same}`
