get_assets_dest <- function() {
  return("assets")
}

setup_server <- function() {
  # ------------------ Setup panflute ------------------- #
  virtualenv_dir = Sys.getenv('VIRTUALENV_NAME')
  python_path = Sys.getenv('PYTHON_PATH')
  dependencies = c('panflute')
  reticulate::virtualenv_create(envname = virtualenv_dir, python = python_path)
  reticulate::virtualenv_install(virtualenv_dir, packages = dependencies)
  reticulate::use_virtualenv(virtualenv_dir, required = TRUE)
  reticulate::py_config()
  
  # ------------------ Setup TeX ------------------- #
  if (!tinytex:::is_tinytex())
    tinytex:::install_prebuilt()
  tinytex::tlmgr_update()
  tinytex::tlmgr_install(c(
      "amsfonts", # identified by (1) running on a freshly installed TinyTeX,
      "amsmath", # (2) by looking for \IfFileExists inside <https://github.com/jgm/pandoc-templates/blob/2.11.4/default.latex>,
      "babel", # (3) by looking at <https://github.com/jgm/pandoc/blob/2.11.4/MANUAL.txt#L136-L162>
      "babel-german", # (4) by looking at the extra_dependencies in all Rmd documents
      "bookmark", # and (5) by looking for \usepackage in all Rmd documents
      "booktabs", # except those that cannot be installed: flafter, graphicx, ifluatex, ifxetex, longtable, xspace
      "csquotes",
      "ctablestack",
      "fancyvrb",
      "fontspec",
      "footnotehyper",
      "forloop",
      "geometry",
      "hyperref",
      "hyphenat",
      "koma-script",
      "listings",
      "lm",
      "luatexbase",
      "mdframed",
      "microtype",
      "needspace",
      "parskip",
      "selnolig",
      "setspace",
      "ulem",
      "unicode-math",
      "upquote",
      "xcolor",
      "xstring",
      "xurl",
      "ziffer",
      "zref",
      "collection-langgerman", # for German (new orthography) hyphenation patterns; source: https://github.com/yihui/tinytex/issues/97#issuecomment-480337364
      "pdfcrop", # for cropping PDF figures; see https://bookdown.org/yihui/rmarkdown/pdf-document.html#figure-options-1
      "xits", # for the XITS Math font
      "haranoaji" # for the CJK fonts
  ))
  tinytex::tlmgr_update()
  
  file_base <- ""
  
  # ------------------ Setup Libertinus font ------------------- #
  if (!file.exists(file.path(get_assets_dest(), ".libertinus_installed"))) {
    tryCatch({
      data <- jsonlite::fromJSON("https://api.github.com/repos/alerque/libertinus/releases/latest")
      file_url <- ""
      for (row in 1:nrow(data$assets)) {
        if (grepl("zip", data$assets[row, "browser_download_url"], fixed = TRUE)) {
          file_url <- data$assets[row, "browser_download_url"]
          break
        }
      }
      
      file_base <- file.path(get_assets_dest(), basename(file_url))
      library(curl) # required for download.file
      download.file(url=file_url, destfile=file_base)
      otf_files <- grep('\\.otf$', unzip(file_base, list=TRUE)$Name, ignore.case=TRUE, value=TRUE)
      unzip(file_base, exdir=get_assets_dest(), junkpaths=TRUE, files=otf_files)
    },
    error=function(cond) {
      write("Obtaining Libertinus from GitHub failed, resorting to local fallback", stderr())
      file_base <- file.path("bin-fallback", list.files("bin-fallback", pattern = "^Libertinus")[[1]])
      otf_files <- grep('\\.otf$', unzip(file_base, list=TRUE)$Name, ignore.case=TRUE, value=TRUE)
      unzip(file_base, exdir=get_assets_dest(), junkpaths=TRUE, files=otf_files)
    })
    file.create(file.path(get_assets_dest(), ".libertinus_installed"))
  }
  
  # ------------------ Setup Raleway font ------------------- #
  if (!file.exists(file.path(get_assets_dest(), ".raleway_installed"))) {
    tryCatch({
      data <- jsonlite::fromJSON("https://api.github.com/repos/theleagueof/raleway/releases/latest")
      file_url <- ""
      for (row in 1:nrow(data$assets)) {
        if (grepl("zip", data$assets[row, "browser_download_url"], fixed = TRUE)) {
          file_url <- data$assets[row, "browser_download_url"]
          break
        }
      }
      file_base <- file.path(get_assets_dest(), basename(file_url))
      download.file(url=file_url, destfile=file_base)
      otf_files <- grep('\\.otf$', unzip(file_base, list=TRUE)$Name, ignore.case=TRUE, value=TRUE)
      unzip(file_base, exdir=get_assets_dest(), junkpaths=TRUE, files=otf_files)
    },
    error=function(cond) {
      write("Obtaining Raleway from GitHub failed, resorting to local fallback", stderr())
      file_base <- file.path("bin-fallback", list.files("bin-fallback", pattern = "^Raleway")[[1]])
      otf_files <- grep('\\.otf$', unzip(file_base, list=TRUE)$Name, ignore.case=TRUE, value=TRUE)
      unzip(file_base, exdir=get_assets_dest(), junkpaths=TRUE, files=otf_files)
    })
    file.create(file.path(get_assets_dest(), ".raleway_installed"))
  }
  
  # ------------------ Setup Source Sans Pro font ------------------- #
  if (!file.exists(file.path(get_assets_dest(), ".source-sans-pro_installed"))) {
    tryCatch({
      data <- jsonlite::fromJSON("https://api.github.com/repos/adobe-fonts/source-sans-pro/releases/latest")
      file_url <- ""
      for (row in 1:nrow(data$assets)) {
        if (grepl("zip", data$assets[row, "browser_download_url"], fixed = TRUE)) {
          file_url <- data$assets[row, "browser_download_url"]
          break
        }
      }
      file_base <- file.path(get_assets_dest(), basename(file_url))
      download.file(url=file_url, destfile=file_base)
      otf_files <- grep('\\.otf$', unzip(file_base, list=TRUE)$Name, ignore.case=TRUE, value=TRUE)
      unzip(file_base, exdir=get_assets_dest(), junkpaths=TRUE, files=otf_files)
    },
    error=function(cond) {
      write("Obtaining Source Sans Pro from GitHub failed, resorting to local fallback", stderr())
      file_base <- file.path("bin-fallback", list.files("bin-fallback", pattern = "^source-sans")[[1]])
      otf_files <- grep('\\.otf$', unzip(file_base, list=TRUE)$Name, ignore.case=TRUE, value=TRUE)
      unzip(file_base, exdir=get_assets_dest(), junkpaths=TRUE, files=otf_files)
    })
    file.create(file.path(get_assets_dest(), ".source-sans-pro_installed"))
  }
  
  # ------------------ Setup Source Code Pro font ------------------- #
  if (!file.exists(file.path(get_assets_dest(), ".source-code-pro_installed"))) {
    tryCatch({
      file_url <- "https://github.com/adobe-fonts/source-code-pro/releases/download/2.030R-ro%2F1.050R-it/source-code-pro-2.030R-ro-1.050R-it.zip"
      
      file_base <- file.path(get_assets_dest(), basename(file_url))
      download.file(url=file_url, destfile=file_base)
      otf_files <- grep('\\.otf$', unzip(file_base, list=TRUE)$Name, ignore.case=TRUE, value=TRUE)
      unzip(file_base, exdir=get_assets_dest(), junkpaths=TRUE, files=otf_files)
    },
    error=function(cond) {
      write("Obtaining Source Code Pro from GitHub failed, resorting to local fallback", stderr())
      file_base <- file.path("bin-fallback", list.files("bin-fallback", pattern = "^source-code-pro")[[1]])
      otf_files <- grep('\\.otf$', unzip(file_base, list=TRUE)$Name, ignore.case=TRUE, value=TRUE)
      unzip(file_base, exdir=get_assets_dest(), junkpaths=TRUE, files=otf_files)
    })
    file.create(file.path(get_assets_dest(), ".source-code-pro_installed"))
  }
  
  # ------------------ Setup Pandoc ------------------- #
  if (!dir.exists(pandoc_home <- path.expand(file.path("~", "pandoc-bin")))) {
    tryCatch({
      data <- jsonlite::fromJSON("https://api.github.com/repos/jgm/pandoc/releases/latest")
      file_url <- ""
      for (row in 1:nrow(data$assets)) {
        if (grepl("amd64.tar.gz", data$assets[row, "browser_download_url"], fixed = TRUE)) {
          file_url <- data$assets[row, "browser_download_url"]
          break
        }
      }
      file_base <- file.path(get_assets_dest(), basename(file_url))
      download.file(url=file_url, destfile=file_base)
      untar(tarfile=file_base, exdir=pandoc_home)
    },
    error=function(cond) {
      write("Obtaining Pandoc from GitHub failed, resorting to local fallback", stderr())
      file_base <- file.path("bin-fallback", list.files("bin-fallback", pattern = "^pandoc")[[1]])
      untar(tarfile=file_base, exdir=pandoc_home)
    })
    file.remove(file_base)
  }
  pandoc_location <- file.path(pandoc_home, list.files(pandoc_home)[[1]], "bin")
  write(paste0("PATH=", pandoc_location, ":$PATH"), file=path.expand(file.path("~", ".profile")), append=TRUE)
  Sys.setenv(PATH = paste(pandoc_location, Sys.getenv("PATH"), sep = ":"))
  rmarkdown::find_pandoc(cache = FALSE) # clear cached pandoc path and search again
  
  # ------------------ Settings ------------------- #
  options(width=75)
  Sys.setlocale('LC_ALL', 'de_DE.UTF-8')
}

render_document <- function(input, params, output_format, output_file = NULL, logfile = NULL) {
  withCallingHandlers({
    capture.output(file = logfile, type=c("output"), append = TRUE, split = TRUE, {
      
      toc <- FALSE
      if (exists("toc", where=params)) {
        toc <- params$toc
      }
      number_sections <- FALSE
      if (exists("number_sections", where=params)) {
        number_sections <- params$number_sections
      }
      if (exists("bibliography_file", where=params)) {
        if (is.null(params$bibliography_file)) {
          bib <- file.path(get_assets_dest(), "bibliography.bib")
          file.create(bib)
        }
      }
      
      out <- rmarkdown::render(input=input,
                               params = params,
                               output_format=output_format,
                               output_file=output_file,
                               envir = new.env(parent = globalenv()),
                               output_options = list(toc=toc, number_sections=number_sections))
    })
    
    return(out)
    
  },
  message = function(m) {
    shinyjs::html(id = "conditions",
                  html = paste0("<div>Message: <samp class=\"shiny-text-output\">",
                                trimws(m$message), "</samp></div>"), add = TRUE)
  },
  warning = function(m) {
    shinyjs::html(id = "conditions",
                  html = paste0("<div>Warning: <samp class=\"shiny-text-output\">",
                                trimws(m$message), "</samp></div>"), add = TRUE)
  },
  error = function(m) {
    shinyjs::html(id = "conditions",
                  html = paste0("<div>Error: <samp class=\"shiny-text-output\">",
                                trimws(m$message), "</samp></div>"), add = TRUE)
  })
}

get_template <- function(session) {
  query <- parseQueryString(session$clientData$url_search)
  if (!is.null(query[['template']])) {
    return(file.path(get_assets_dest(), query[['template']]))
  } else {
    return(file.path(get_assets_dest(), "default.Rmd"))
  }
}
