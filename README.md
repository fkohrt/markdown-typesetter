# Markdown Typesetter

An online service for converting Markdown documents to PDF and HTML. Always using latest Pandoc and LuaLaTeX.

## Usage

Go to [`fkohrt.shinyapps.io/markdown-typesetter/`](https://fkohrt.shinyapps.io/markdown-typesetter/), enter the metadata and upload your document.

Alternatively run the following locally:

```r
setwd("markdown-typesetter")
source(".Rprofile")
source("helper_functions.R")
setup_server()
```

Then one can either spin up __rmarkdown__'s built-in parameter user interface (`knit_params_ask`):

```r
rmarkdown::render(file.path("assets", "default.Rmd"), params = "ask",
                  output_format = "bookdown::pdf_document2",
                  output_options = list(toc = FALSE, number_sections = FALSE))
```

Or one can run completely headless using the following command:

```r
rmarkdown::render(file.path("assets", "default.Rmd"),
                  params = list(
                                title = "",
                                subtitle = "",
                                author = "",
                                author_url = "",
                                date = format(Sys.time(),
                                              format='%e. %B %Y',
                                              tz='Europe/Berlin'),
                                lang = "de-DE",
                                version = "0.0.1",
                                work_url = "",
                                license = "CC-BY-SA-4.0",
                                cjk_font = FALSE,
                                show_urls_in_footnotes = FALSE,
                                show_url_break_symbols = FALSE,
                                twocolumn = FALSE,
                                draft = FALSE,
                                about = TRUE,
                                body_file = NULL,
                                bibliography_file = NULL),
                  output_format="bookdown::pdf_document2",
                  output_options = list(toc = FALSE, number_sections = FALSE))
```

Here's a list of references for writing your document ordered by increasing specificity:

1. Your document needs to be written in [Markdown](https://commonmark.org/help/).
2. To be more precise, you can use all features of [Pandoc’s Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown).
3. Additionally, you can make use of the [Markdown extensions by bookdown](https://bookdown.org/yihui/rmarkdown/bookdown-markdown.html).
4. It is also possible to execute R code by writing full-fledged [R Markdown documents](https://rmarkdown.rstudio.com/articles_intro.html).
5. Finally, you can use the language of your target format – [HTML](https://developer.mozilla.org/docs/Learn/HTML) or PDF via [LaTeX](https://www.learnlatex.org/) – by writing [raw blocks](https://pandoc.org/MANUAL.html#extension-raw_attribute).

## Contributing

See [CONTRIBUTING.md](https://gitlab.com/fkohrt/markdown-typesetter/-/blob/main/CONTRIBUTING.md).

## Credits

- Gregor de Cillia's [Shiny module for `knit_params_ask`](https://gist.github.com/GregorDeCillia/3601ebaf8f285d4e37bfd8f69c8f4b4f)
- Gonzalo Medina's [title page layout](https://tex.stackexchange.com/a/157333) (for some more inspiration, see [Showcase of beautiful title page done in TeX](https://tex.stackexchange.com/q/85904) and [Code improvement on a title page design](https://tex.stackexchange.com/q/156076))
